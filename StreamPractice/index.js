'use strict';
const fs = require('fs');
const fsExtra = require('fs-extra')

const api = require("./modules/api.js");
const Rules = api.Rules;

const testHTMLPath = './html/test.html';


let testScanFromReadableOutputWritable = ()=> {
    const readable = fs.createReadStream(testHTMLPath);
    let newRule = () => {
        return 'new Rule!!! : testScanFromReadableOutputWritable';
    };
    fsExtra.ensureFileSync('./text/ScanFromReadableOutputWritable.txt')
    const writable = fs.createWriteStream('./text/ScanFromReadableOutputWritable.txt');//process.stdout

    let rules = [Rules.rule1 , Rules.rule2, Rules.rule3, Rules.rule4(15), Rules.rule5, newRule];

    api.scanFromReadableOutputWritable(readable, writable, rules );
};

let testScanFromReadableOutputFile = ()=>{
    const readable = fs.createReadStream(testHTMLPath);
    let newRule = () => {
        return 'new Rule!!! : testScanFromReadableOutputFile';
    };
    let rules = [Rules.rule1 , Rules.rule2, Rules.rule3, Rules.rule4(15), Rules.rule5, newRule];
    api.scanFromReadableOutputFile( readable ,'./text/ScanFromReadableOutputFile.txt', rules )
};

let testScanFromReadableOutputConsole = () => {
    const readable = fs.createReadStream(testHTMLPath);
    let newRule = () => {
        return 'new Rule!!! : testScanFromReadableOutputConsole';
    };
    let rules = [Rules.rule1 , Rules.rule2, Rules.rule3, Rules.rule4(15), Rules.rule5, newRule];
    api.scanFromReadableOutputConsole(readable, rules)
};

let testScanFromFileOutputFile = () =>{
    let newRule = () => {
        return 'new Rule!!! : testScanFromFileOutputFile';
    };
    let rules = [Rules.rule1 , Rules.rule2, Rules.rule3, Rules.rule4(15), Rules.rule5, newRule];
    api.scanFromFileOutputFile(testHTMLPath,'./text/ScanFromFileOutputFile.txt', rules);
};

let testScanFromFileOutputWritable = () => {
    let newRule = () => {
        return 'new Rule!!! : testScanFromFileOutputWritable';
    };
    const writable = fs.createWriteStream('./text/ScanFromFileOutputWritable.txt');
    let rules = [Rules.rule1 , Rules.rule2, Rules.rule3, Rules.rule4(15), Rules.rule5, newRule];
    api.scanFromFileOutputWritable(testHTMLPath, writable, rules);
};

let testScanFromFileOutputConsole = () => {
    let newRule = () => {
        return 'new Rule!!! : testScanFromFileOutputConsole';
    };
    let rules = [Rules.rule1 , Rules.rule2, Rules.rule3, Rules.rule4(15), Rules.rule5, newRule];
    api.scanFromFileOutputConsole(testHTMLPath, rules);
};
console.log("===testScanFromReadableOutputWritable start")
testScanFromReadableOutputWritable();
console.log("===testScanFromReadableOutputWritable end")

console.log("===testScanFromReadableOutputFile start")
testScanFromReadableOutputFile();
console.log("===testScanFromReadableOutputFile end")

console.log("===testScanFromReadableOutputConsole start")
testScanFromReadableOutputConsole();
console.log("===testScanFromReadableOutputConsole end")

console.log("===testScanFromFileOutputFile start")
testScanFromFileOutputFile();
console.log("===testScanFromFileOutputFile end")

console.log("===testScanFromFileOutputWritable start")
testScanFromFileOutputWritable();
console.log("===testScanFromFileOutputWritable end")

console.log("===testScanFromFileOutputConsole start")
testScanFromFileOutputConsole();
console.log("===testScanFromFileOutputConsole end")