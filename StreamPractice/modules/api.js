'use strict';
const fs = require('fs');
const fsExtra = require('fs-extra');

const Handler = require("./handler.js");
const Rules = require("./rules.js");

const defaultRules = [Rules.rule1, Rules.rule2, Rules.rule3, Rules.rule4, Rules.rule5];

let scanFromReadableOutputWritable = (readable, writable, applyRules) => {
    let htmlData = '';
    readable.on('data', (chunk) => {
        htmlData = htmlData + chunk;
    });

    readable.on('end', () => {
        let handler = chainHandlers(htmlData, writable, applyRules);
        if (handler) handler.HandleRequest();
    });
};

let scanFromReadableOutputFile = (readable, filePath, applyRules) => {
    fsExtra.ensureFileSync(filePath);
    const writable = fs.createWriteStream(filePath);//process.stdout

    let htmlData = '';
    readable.on('data', (chunk) => {
        htmlData = htmlData + chunk;
    });

    readable.on('end', () => {
        let handler = chainHandlers(htmlData, writable, applyRules);
        if (handler) handler.HandleRequest();
        writable.end();
    });
};

let scanFromReadableOutputConsole = (readable, applyRules) => {
    const writable = process.stdout;
    let htmlData = '';
    readable.on('data', (chunk) => {
        htmlData = htmlData + chunk;
    });

    readable.on('end', () => {
        let handler = chainHandlers(htmlData, writable, applyRules);
        if (handler) handler.HandleRequest();
    });
};

let scanFromFileOutputFile = (scanFilePath, outputFilePath, applyRules) => {
    const readable = fs.createReadStream(scanFilePath);
    const writable = fs.createWriteStream(outputFilePath);
    let htmlData = '';
    readable.on('data', (chunk) => {
        htmlData = htmlData + chunk;
    });
    readable.on('end', () => {
        let handler = chainHandlers(htmlData, writable, applyRules);
        if (handler) handler.HandleRequest();
        writable.end();
    });
};

let scanFromFileOutputWritable = (scanFilePath, writable, applyRules) => {
    const readable = fs.createReadStream(scanFilePath);
    let htmlData = '';
    readable.on('data', (chunk) => {
        htmlData = htmlData + chunk;
    });
    readable.on('end', () => {
        let handler = chainHandlers(htmlData, writable, applyRules);
        if (handler) handler.HandleRequest();
    });
};

let scanFromFileOutputConsole = (scanFilePath, applyRules) => {
    const readable = fs.createReadStream(scanFilePath);
    const writable = process.stdout;
    let htmlData = '';
    readable.on('data', (chunk) => {
        htmlData = htmlData + chunk;
    });
    readable.on('end', () => {
        let handler = chainHandlers(htmlData, writable, applyRules);
        if (handler) handler.HandleRequest();
    });
};

let chainHandlers = (htmlData, writable, applyRules) => {
    let handlers = [];
    applyRules.forEach(function (item, index, array) {
        handlers.push(new Handler(htmlData, writable, item));
        if (index - 1 >= 0) {
            handlers[index - 1].setSuccessor(handlers[index]);
        }
    });
    return handlers[0];
};

module.exports = {
    scanFromReadableOutputWritable,
    scanFromReadableOutputFile,
    scanFromReadableOutputConsole,
    scanFromFileOutputFile,
    scanFromFileOutputWritable,
    scanFromFileOutputConsole,
    Rules
};