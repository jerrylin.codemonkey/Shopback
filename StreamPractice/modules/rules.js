const htmlparser = require("htmlparser2");
const cheerio = require('cheerio')

let rule1 = (htmlData) => {
    let output = "";
    const tag = "img";
    let parser = new htmlparser.Parser({
        onopentag: function (name, attribs) {
            if (name === tag) {
                if (!attribs.alt) {
                    output = output + `There are 1 <${tag}> tag without alt attribute` + '\n';
                }
            }
        }
    }, {decodeEntities: true});
    parser.write(htmlData);
    parser.end();
    return output.length > 0 ? output : null;
};


let rule2 = (htmlData) => {
    let output = "";
    const tag = "a";
    let parser = new htmlparser.Parser({
        onopentag: function (name, attribs) {
            if (name === tag) {
                if (!attribs.rel) {
                    output = output + `There are 1 <${tag}> tag without rel attribute` + '\n';
                }
            }
        }
    }, {decodeEntities: true});
    parser.write(htmlData);
    parser.end();
    return output;
};

let rule3 = (htmlData) => {
    let output = "";
    const $ = cheerio.load(htmlData);
    $('head').each(function (i, elem) {
        if ($(this).has('title').length === 0) {
            output = output + 'Header doesn’t have <title> tag' + '\n';
        }

        let hasMetaNameDescriptions = false;
        let hasMetaNameKeywords = false;
        $(this).find('meta').each(function (i, elem) {
            if ($(this).attr('name') === 'descriptions') {
                hasMetaNameDescriptions = true;
            }
            if ($(this).attr('name') === 'keywords') {
                hasMetaNameKeywords = true;
            }
        });

        if (!hasMetaNameDescriptions) {
            output = output + 'Header doesn’t have <meta name=“descriptions” ... /> tag' + '\n';
        }

        if (!hasMetaNameKeywords) {
            output = output + 'header doesn’t have <meta name=“keywords” ... /> tag' + '\n';
        }

    });

    return output;
};


let rule4 = (tags) => {

    return (htmlData) => {
        let tagCount = 0;
        let output = "";
        const tagExpected = tags ? tags : 15;
        const tag = "strong";
        let isOut = false;
        let parser = new htmlparser.Parser({
            onopentag: function (name, attribs) {
                if (name === "strong") {
                    tagCount++;
                    if (tagCount > tagExpected && !isOut) {
                        output = output + `This HTML have more than ${tagExpected} <${tag}> tag` + "\n";
                        isOut = true;
                    }
                }
            }
        }, {decodeEntities: true});
        parser.write(htmlData);
        parser.end();
        return output;
    }
};

let rule5 = (htmlData) => {
    let tagCount = 0;
    let output = "";
    const tagExpected = 1;
    const tag = "h1";
    let isOut = false;
    let parser = new htmlparser.Parser({
        onopentag: function (name, attribs) {
            if (name === tag) {
                tagCount++;
                if (tagCount > tagExpected && !isOut) {
                    output = output + `This HTML have more than ${tagExpected} <${tag}> tag` + "\n";
                    isOut = true;
                }
            }
        }
    }, {decodeEntities: true});
    parser.write(htmlData);
    parser.end();
    return output;
};

module.exports = {
    rule1,
    rule2,
    rule3,
    rule4,
    rule5,
};