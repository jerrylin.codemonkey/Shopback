'use strict';

class Handler {

    constructor(htmlData, writableStream, applyFunc) {
        this.htmlData = htmlData
        this.writableStream = writableStream
        this.applyFunc = applyFunc
    }

    HandleRequest() {
        var outputText = this.applyFunc(this.htmlData);
        this.writeToStream(outputText);
        this.processSuccessor()
    }

    writeToStream(chunck) {
        if (chunck) {
            this.writableStream.write(chunck + '\n');
        }
    }

    setSuccessor(successor) {
        this.successor = successor
    }

    processSuccessor() {
        if (this.successor != null) {
            this.successor.HandleRequest()
        }
    }
}


module.exports = Handler