const seo = require('@jerrycodemonkey/seo');
const Rules = seo.Rules;
const fs = require('fs');
const fsExtra = require('fs-extra');
const testHTMLPath = './html/test.html';

let testScanFromReadableOutputWritable = () => {
    const readable = fs.createReadStream(testHTMLPath);
    let newRule = () => {
        return 'new Rule!!! : testScanFromReadableOutputWritable';
    };
    fsExtra.ensureFileSync('./text/ScanFromReadableOutputWritable.txt')
    const writable = fs.createWriteStream('./text/ScanFromReadableOutputWritable.txt');//process.stdout
    let rules = [Rules.rule1, Rules.rule2, Rules.rule3, Rules.rule4(15), Rules.rule5, newRule];
    seo.scanFromReadableOutputWritable(readable, writable, rules);
};

let testScanFromReadableOutputFile = () => {
    const readable = fs.createReadStream(testHTMLPath);
    let newRule = () => {
        return 'new Rule!!! : testScanFromReadableOutputFile';
    };
    let rules = [Rules.rule1, Rules.rule2, Rules.rule3, Rules.rule4(15), Rules.rule5, newRule];
    seo.scanFromReadableOutputFile(readable, './text/ScanFromReadableOutputFile.txt', rules)
};

let testScanFromReadableOutputConsole = () => {
    const readable = fs.createReadStream(testHTMLPath);
    let newRule = () => {
        return 'new Rule!!! : testScanFromReadableOutputConsole';
    };
    let rules = [Rules.rule1, Rules.rule2, Rules.rule3, Rules.rule4(15), Rules.rule5, newRule];
    seo.scanFromReadableOutputConsole(readable, rules)
};

let testScanFromFileOutputFile = () => {
    let newRule = () => {
        return 'new Rule!!! : testScanFromFileOutputFile';
    };
    let rules = [Rules.rule1, Rules.rule2, Rules.rule3, Rules.rule4(15), Rules.rule5, newRule];
    seo.scanFromFileOutputFile(testHTMLPath, './text/ScanFromFileOutputFile.txt', rules);
};

let testScanFromFileOutputWritable = () => {
    let newRule = () => {
        return 'new Rule!!! : testScanFromFileOutputWritable';
    };
    const writable = fs.createWriteStream('./text/ScanFromFileOutputWritable.txt');
    let rules = [Rules.rule1, Rules.rule2, Rules.rule3, Rules.rule4(15), Rules.rule5, newRule];
    seo.scanFromFileOutputWritable(testHTMLPath, writable, rules);
};

let testScanFromFileOutputConsole = () => {
    let newRule = () => {
        return 'new Rule!!! : testScanFromFileOutputConsole';
    };
    let rules = [Rules.rule1, Rules.rule2, Rules.rule3, Rules.rule4(15), Rules.rule5, newRule];
    seo.scanFromFileOutputConsole(testHTMLPath, rules);
};
testScanFromReadableOutputWritable();

testScanFromReadableOutputFile();

testScanFromReadableOutputConsole();

testScanFromFileOutputFile();

testScanFromFileOutputWritable();

testScanFromFileOutputConsole();